# Import de bibliothèques
import flask
from flask import request, jsonify, render_template

# Création de l'objet Flask
app = flask.Flask(__name__)
app.config["DEBUG"] = True
# Quelques données tests pour l’annuaire sous la forme d’une liste de dictionnaires
employees = [
    {"id": 0, "Nom": "Dupont", "Prenom": "Jean", "Fonction": "Developpeur"},
    {"id": 2, "Nom": "Doe", "Prenom": "John", "Fonction": "Testeur"},
]


@app.route("/", methods=["GET"])
def home():
    return render_template("index.html")


@app.route("/api/v1/resources/employees/all", methods=["GET"])
def api_all():
    return jsonify(employees)


@app.route("/api/v1/resources/employees", methods=["GET"])
def api_id():
    # Vérifie si un ID est fourni dans une URL.
    # Si un ID est fourni, il est affecté à une variable.
    # Si aucun ID n’est fourni, un message d’erreur est affiché dans le navigateur.
    if "id" in request.args:
        id = int(request.args["id"])
    else:
        return "Erreur: Pas d’identifiant fourni. Veuillez spécifier un id."

    # Crée une liste vide pour stocker les résultats
    results = []

    # Boucle sur les données pour obtenir les résultats correspondant à l’ID fourni.
    # Les IDs sont uniques, mais les autres champs peuvent renvoyer plusieurs résultats
    for employee in employees:
        if employee["id"] == id:
            results.append(employee)

    # La fonction jsonify de Flask convertit notre liste de dictionnaires Python
    # au format JSON     return jsonify(results)


app.run()
